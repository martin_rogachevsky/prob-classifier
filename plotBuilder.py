import matplotlib.pyplot as plt
import numpy as np

def drawResults(func1, func2, vectSet):
    plt.plot(vectSet, func1(vectSet))
    plt.plot(vectSet, func2(vectSet))
    y1 = func1(vectSet)
    y2 = func2(vectSet)
    axis = plt.axes()
    axis.fill_between(vectSet, 0, y1, where=y2 >= y1)
    axis.fill_between(vectSet, 0, y2, where=y1 >= y2)
    plt.show()
