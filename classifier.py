from random import gauss
from statistics import mean, stdev
from math import pi
import numpy as np

STEP = 0.0001
VECT_COUNT = 500

class Classifier:
    def __init__(self, prob, gauss1, gauss2):
        self.prob = prob
        self.gauss = [gauss1, gauss2]
        self.func1 = Classifier.getGaussSpread(gauss1, prob)
        self.func2 = Classifier.getGaussSpread(gauss2, 1-prob)
        self.classify()

    def classify(self):
        integralArea = Classifier.integralArea(self.gauss[0], self.gauss[1])
        self.detectionMistake = 0
        self.falsePositive = 0
        self.integralSet = np.arange(integralArea[0], integralArea[1], STEP)
        for x in list(self.integralSet):
            val1 = self.func1(x)
            val2 = self.func2(x)
            if val2 < val1:
                self.detectionMistake += val2 * STEP
            else:
                self.falsePositive += val1 * STEP

    @staticmethod
    def getGaussSpread(gauss, prob):
        return lambda x: 1/(gauss[1]*np.sqrt(2*pi))*np.exp(-0.5*((x-gauss[0])/gauss[1])**2) * prob

    @staticmethod
    def integralArea(gauss1, gauss2):
        vectors = Classifier.vectors(gauss1) + Classifier.vectors(gauss2)
        return min(vectors), max(vectors)
    
    @staticmethod
    def vectors(gaussParams):
        result = list()
        for _ in range(VECT_COUNT):
            result.append(gauss(gaussParams[0], gaussParams[1]))
        return result




