from sys import argv
from classifier import Classifier
from plotBuilder import drawResults

def mistakes(detectionMistake, falsePositive):
    return 'Pл.т.=' + '{0:.2f}%'.format(detectionMistake*100) + '\nPп.о.=' + '{0:.2f}%'.format(falsePositive*100) + \
        '\nPо.=' + '{0:.2f}%'.format((falsePositive+detectionMistake)*100)


def main():
    classifier = Classifier(float(argv[1]), (float(argv[2]), float(argv[3])), (float(argv[4]), float(argv[5])))
    detectionMistake, falsePositive = classifier.detectionMistake, classifier.falsePositive
    print(mistakes(detectionMistake, falsePositive))
    drawResults(classifier.func1, classifier.func2, classifier.integralSet)

main()
